Welcome to the ErgoAI bug tracker!

To report an issue:

* please log in (you can create a free account on Bitbucket)
* click on [Issues](https://bitbucket.org/ergoaitrackingteam/ergoai-issue-tracker/issues?status=new&status=open) in the tool bar on the left (the square with a dot inside), and
* create a new report (click on [+Create issue](https://bitbucket.org/ergoaitrackingteam/ergoai-issue-tracker/issues/new)).

Please describe the problem and supply an easily reproducible test case. The test case should include a simple set of instructions, possibly accompanied by a relevant Ergo snipplet.